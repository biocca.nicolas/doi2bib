# DOI2BIB 

This is a CLI-based Python script to resolve Digital Object Identifier (DOI) 
into BibTeX entries. It's based on the exposed API at [doi.org](https://www.doi.org/).

Referece:
 - [DOI to BibTeX @ scipython](https://scipython.com/blog/doi-to-bibtex/)

## TODO

  - [ ] virtual environments
  - [ ] dockerize
  - [ ] unit test
  - [ ] Improve REDME (this one!)
 
